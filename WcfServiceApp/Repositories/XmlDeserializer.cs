﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Repositories
{
    public static class XmlDeserializer
    {
        public static XmlElements DeserializeXml<T>() where T: class
        {
            XmlElements response = null;
            string text = File.ReadAllText(Environment.CurrentDirectory + "\\bin\\Debug\\net5.0\\XMLFile\\DBConnection.xml");
            MemoryStream MemStream = new MemoryStream();
            byte[] b = Encoding.UTF8.GetBytes(text);
            MemStream.Write(b, 0, b.Length);
            XmlDocument XmlDoc = new XmlDocument();
            XmlDoc.LoadXml(UnicodeEncoding.UTF8.GetString(MemStream.ToArray()));

            string aciResponseData = XmlDoc.InnerXml;
            using (TextReader sr = new StringReader(aciResponseData))
            {
                var serializer = new XmlSerializer(typeof(XmlElements));
                response = (XmlElements)serializer.Deserialize(sr);
            }
            return response;
        }
    }
}
