﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WcfServiceApp.Models;

namespace WcfServiceApp
{
   public class MainService : IMainService
   {

        public User[] GetAllUser()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            IEnumerable<User> users = context.Users;
            return users.ToArray();
        }

        public User GetUserBy(int id)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            IQueryable<User> users = context.Users.Where();
            return users.FirstOrDefault();
        }

        public string Message()
        {
            return "Test";
        }
    }
}
