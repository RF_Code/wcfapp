﻿using Repositories.Model;
using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Repositories
{
    [XmlRoot("Elements", IsNullable = false)]
    public class XmlElements
    {
        [XmlElement("ConnectionString")]
        public ConnectionString ConnectionString { get; set; }
    }
}
