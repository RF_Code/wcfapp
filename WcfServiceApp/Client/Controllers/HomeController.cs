﻿using Client.Models;
using DbConnection;
using MainWebServiceReference;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;

namespace Client.Controllers
{
    public class HomeController : Controller
    { 
        MainServiceClient client = new MainServiceClient();
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            ViewBag.Message = client.MessageAsync().Result;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SaveRegisterDetails(Models.User registerDetails)
        {
            if(registerDetails != null)
            {
                DbHandle db = new DbHandle();
                DbConnection.Models.User user = new DbConnection.Models.User();
                user.Name = registerDetails.Name;
                user.SecondName = registerDetails.SecondName;
                db.Add(user);
            }
            
            return Ok();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
