﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Repositories.Model
{
    public class ConnectionString
    {
        [XmlElement("Connection")]
        public Connection Connection { get; set; }
    }

    public class Connection
    {
        [XmlAttribute("name")]
        public string Name { get; set; }
        [XmlAttribute("connectionString")]
        public string ConnectionString { get; set; }
        [XmlAttribute("providerName")]
        public string ProviderName { get; set; }
    }
}
