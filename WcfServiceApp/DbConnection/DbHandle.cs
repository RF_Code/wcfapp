﻿using DbConnection.Models;
using Microsoft.EntityFrameworkCore;
using Repositories;
using System;
using System.Data.SqlClient;

namespace DbConnection
{
    public class DbHandle : DbContext
    {
        public void Add(User user)
        {
            var elemets = XmlDeserializer.DeserializeXml<XmlElements>();
            using (SqlConnection sqlConnection = new SqlConnection(elemets.ConnectionString.Connection.ConnectionString))
            {
                sqlConnection.Open();
                sqlConnection.BeginTransaction();
                ApplicationDbContext dbContext = new ApplicationDbContext();
                dbContext.Users.Add(user);
                dbContext.SaveChanges();
                sqlConnection.Close();
                
            }
        }
    }
}
