﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WcfServiceApp.Models;

namespace WcfServiceApp
{
    [ServiceContract]
    public interface IMainService
    {
        [OperationContract]
        User[] GetAllUser();

        [OperationContract]
        User GetUserBy(int id);

        [OperationContract]
        string Message();
    }
}
